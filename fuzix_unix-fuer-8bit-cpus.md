Title: FUZIX: Unix für 8-Bit-CPUs, erster Teil
Date: 2023-02-27 18:12
Modified: 2023-04-30 11:40
Category: Retrocomputing
Tags: z80, NC100, FUZIX
Slug: FUZIX_Unix-fuer-8-Bit-CPUs
Author: Reiko Kaps
Summary: Ein Unix auf einer 8-Bit-CPU und in weniger als 1 Mbyte Speicher
Status: 
Featured_Image: /images/fuzix-nc100em.png

Vor gut 9 Jahren hat der bekannte Linux-Entwickler Alan Cox unter dem
Motto "Because Small Is Beautiful" das [FUZIX-Projekt](https://github.com/EtchedPixels/FUZIX) gestartet. FUZIX
setzt seinerseits auf Vorarbeiten von
[UZI](https://github.com/chettrick/uzics) auf. Für Cox verschmelzen in
FUZIX verschiedene UZI-Forks "zu einer halbwegs kohärenten Plattform,
die anschließend von V7 auf einen Bereich zwischen SYS3 und SYS5.x
erweitert wurde - einige POSIX-Elemente kommen noch als Zugabe
drauf. Zusätzlich wurden noch einige Tricks und Lehren aus der
Entwicklung von [ELKS](https://github.com/jbruchon/elks) und
[OMU](http://www.pix.net/mirrored/discordia.org.uk/~steve/omu.html)
eingebaut." [1]

FUZIX unterstützt eine Reihe von alten und neuen
Computer(systemen). Dazu zählt etwa der Amstrad NC100 oder Sinclair ZX
Spectrum 128K, weitere finden sich in der
[FUZIX-Dokumentation](https://github.com/EtchedPixels/FUZIX/blob/master/docs/Platforms.md). An
[einigen anderen
Plattformen](https://github.com/EtchedPixels/FUZIX/blob/master/docs/DevPlatforms.md)
wird gerade gearbeitet.

Ich selbst besitze seit einigen Jahren den NC100, den ich mitttels
SRAM-PCMCIA-Karten auf 1 MByte RAM erweitert habe. Damit wäre es laut
FUZIX-Dokumentation möglich, das OS auf dem Amstrad laufen zu
lassen. Allerdings müssen davor noch einige Vorarbeiten erledigt
werden.

## FUZIX from Scratch

Zuerst benötigen wir eine brauchbare Build-Umgebung für FUZIX. Alan
Cox beschreibt im
[INSTALL-File](https://github.com/EtchedPixels/FUZIX/blob/master/INSTALL)
das nur knapp, daher habe ich mich entschlossen es hier ausführlicher
und nachvollziehbar dazustellen: Empfehlenswert ist ein frisch
installiertes Debian- oder Ubuntu-Linux - vorzugsweise als virtuelle
Maschine oder Container. Letzteres hat schlicht den Vorteil, dass man
Fehler einfacher findet und erneute Versuche leichter umzusetzen sind.

Alternativ kann man auf die Container-Technik Docker dabei setzen. Sie
läuft praktisch auf allen aktuellen Desktop-Betriebsystemen und ist
dabei besonders einfach: Sämtliche Vorgaben und Einstellungen lassen
sich vorab in einen "Dockerfile" sammeln. Anschließend baut Docker
daraus dann ein Container-Image, über das man dann recht einfach FUZIX
bauen (lassen) kann. Wie man Docker auf dem eigenen Betriebsystem
installiert, [beschreiben die Entwickler
ausführlich](https://docs.docker.com/engine/install/).

Anschließend lädt man sich mein
[Dockerfile](https://codeberg.org/reik_kaps/fuzix-builder/src/branch/main/Dockerfile)
auf den eigenen Rechner (alternativ klont man das
[Repo](https://codeberg.org/reik_kaps/fuzix-builder)), speichert es in
einem separaten Verzeichnis und führt dort folgendes Docker-Kommandos
aus:

    $> docker build

Der Befehl erzeugt aus dem Dockerfile ein Docker-Image, das die
FUZIX-Build-Umgebung sowie einige zusätzliche Skripte enthält. Dazu
zählt das Skript build.sh, über das sich die FUZIX-Sourcen lokale
klonen lassen und das den eigentlichen Build teilweise oder
vollständig automatisiert (vgl. [Listing](https://codeberg.org/reik_kaps/fuzix-builder/src/branch/main/build.sh)).

Das im Docker-Image enthaltene Skript klont die FUZIX-Sourcen auf den
eigenen Rechner, schreibt im Makefile die Zielplattform auf
"amstardnc/nc100" um und übersetzt dann den FUZIX-Kernel sowie die
Disk-Images. Über den Schalter **info** bekommt man zudem einige,
weitere Hinweise zu Dateipfaden und Programm-Parametern.


Ist der Build ohne schwerwiegende Fehler nach einigen Stunden zu einem
Ende gekommen, lässt sich FUZIX bereits testen - falls man den
Emulator nc100em übersetzt und ggf. installiert hat: 

Zuerst muss man das FUZIX-PCMCIA-Card-Image
($PWD/Images/amstradnc/nc100/pcmcia.img) nach $HOME/nc100/nc100.card
kopieren, anschließend ruft man die Kommandozeile 

	$> dnc100em -p $PWD/Kernel/platform-amstradnc/nc100/nc100emu.bin

auf, dnc100em muss dabei im Suchpfad liegen! Als Ergebniss sollte nun
FUZIX starten, was in etwa so wie in diesem
[Video](https://files.mastodon.social/media_attachments/files/109/904/156/916/544/734/original/202c2435e887d9da.mp4)
aussehen sollte:

![FUZIX Startup](images/fuzix-nc100em.png)

Nun kann man sich an die FUZIX-Installation auf der tatsächlichen
Hardware machen. Bislang gibt es aber noch Hürden, die noch nicht
genommen wurden. Dazu gehört die Verbereitung der SRAM-Karte, was ich
aber in einem weiteren Beitrag beschreiben werde.

[1] https://de.wikipedia.org/wiki/Unix
