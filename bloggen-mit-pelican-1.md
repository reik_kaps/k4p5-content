Title: Bloggen mit Pelican 
Date: 2018-04-09 16:00
Authors: Reiko Kaps
Category: Web
Tags: Software, Web, Python, HowTo
Slug: bloggen-mit-pelican
Summary: Static Site Generatoren helfen private Weblogs sicher selbst zu betreiben. Die Software Pelican gehört dazu und lässt sich leicht nutzen. 
featured_image: /images/pelican.png

Zugegeben: Ich bin nicht der Hochfrequenz-Blogger. Daher fristet mein
Blog eher ein Schattendarsein und wird von mir selten beäugt. Obwohl
dort kein Wordpress läuft, kann auch dort laufende einfache
PHP-Blogsoftware durchaus ein Einfallstor für Angreifer sein.

Sicherer und wartungsärmer wäre also eine (vollkommen) statische
Webseite. Damit sich das ganze trotzdem gut verwalten lässt, muss
etwas her, was ich bereits in den späten 90er Jahren eingesetzt habe:
Ein Webseiten-Generator ähnlich der alten und längst nicht mehr
weiterentwickelten [WML](https://thewml.github.io/).

Inzwischen gibt es eine ganze Reihe von Static-Site-Generatoren in
aller Herren Sprachen, die ganz ähnliches übernehmen und dank moderner
Web-Techniken wie HTML5 und CSS sogar einfacher arbeiten. Die in
Python geschriebene Software Pelican oder auch das recht neue Ivy
gehören zu dieser Kategorie.

[Pelican](https://blog.getpelican.com) generiert aus in Ordnern
strukturierten und mit Zusatz-Infos versehenen Text-Dateien (ReSt,
Markdown) einen Website im Stil eines Blogs. Dazu stellt es einige
Commandline-Tools bereit, die beim Schreiben und Generieren der Seite
helfen. Zudem bringt es Funktionen mit, um die erzeugten Dateien auf
dem Webserver oder auch auf Cloud-Diensten abzulegen.

## Funktionen

Die Software generiert zudem RSS-Feeds aus den Blogartikeln und die
Ausgaben lassen sich mit Vorlagen anpassen. Fehlen trotzdem Funktionen
lassen sich diese mit Plugins nachrüsten oder so selbst
programmieren. Für seine Vorlagen verwendet Pelican die unter Python
sehr verbreitete Template-Engine Jinja2, mit der sich bereits sehr
vieles ohne eine einzige Zeile Python-Code bauen lässt. Will man
zusätzlich seine Webseite mit Unterseiten stärker strukturieren, lässt
sich auch das mit Pelican umsetzen. Details zu den Interna von Pelican
verrät die ausführliche Dokumentation.  

## Einrichtung

Nach der Installation von Pelican fragt das Kommando
pelican-quickstart alles nötige für die Einrichtung einer
Pelican-Umgebung ab, schreibt diese in Konfigurationsdateien und
erzeugt Skripte für Generierung und Auslieferung. Wer seine Blog-Texte
lieber in Markdown-Notation schreiben möchte, installiert sich gleich
das dafür nötige Python-Modul nach. Ansonsten erwartet Pelican die
Text als reStructuredText, das etwas komplizierter aber auch mächtiger
als die Auszeichnung in Markdown-Notation ist.

Pelicans Schnelleinrichtung legt zudem die Ordner for die Quell- und
die fertigen HTML-Dateien an (content/ sowie output/). Blog-Artikel
legt man einfach in die oberste Ebene des Verzeichnisses output/,
Pelican erzeigt daraus eine Übersicht samt Vorschautext, Tags und
Datum. Braucht man Webseiten für das Impressum oder Inhalte, die eher
dauerhaften Charakter haben, erstellt man unterhalb von content/ das
Verzeichnis pages/ und plaziert dort seine Dateien: 

![Pelicans Verzeichnisstruktur](/images/pelican-content-folder.jpg)

Anschließend erweitert man Pelicans Konfig-Datei pelicanconf.py um
eine Eintrag, der folgendem Muster folgt:

	LINKS = (('Home', '/'),
	('Vita', '/pages/vita.html'),
	('LeineLab', 'https://leinelab.org/'),
	('Python.org', 'http://python.org/'),
	('Django', 'https://www.djangoproject.com/'),
	)

Der Eintrag Vita verweist hier auf die Datei /pages/vita.html, die
anderen Einträge auf die Startseite oder externe Adressen.  Inhalte
erstellen

Wenn nicht anders konfiguriert, erwartet Pelican seine Quelldateien
als reStructuredText. Ein einfache Vorlage folgt dabei dem Muster

	Bloggen mit Pelican
	###################
	:date: 2018-04-09 18:00
	:tags: Software, Web
	:slug: pelican-site-generator

	...

	Sicherer und wartungsarm wäre also eine (vollkommen) statische
	Webseite. Damit sich das ganze trotzdem gut verwalten lässt, muss
	etwas her, was ich bereits in den späten 90er Jahren eingesetzt habe:
	Ein Webseiten-Generator ähnlich der alten und längst nicht mehr
	weiterentwickelten `WML <http://www.thewml.org/>`_.

Der Kopf der Datei enthält dabei den Artikel- oder Seitentitel sowie
Angaben zum Erstellungsdatum (:date:), den Tags sowie dem Slug - also
dem Datei- bzw. Basename der HTML-Datei. Weitere Erfahrungen und
Details folgen in einem weiteren Post, der in den kommenden Tage
online geht.
