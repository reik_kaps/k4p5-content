Title: Analoge Filme digitalisieren (2/2)
Date: 2022-01-16 16:00:00
Modified: 2022-08-24 11:01
Category: fotografie 
Tags: photo, digitalisierung, archivierung
Slug: analoge-fotos-digitalisieren_2
Author: Christian Hintz
Editor: Reiko Kaps
Summary: Alte Negative oder Dias lassen sich mit speziellen Scanner digitaliseren, die sind entweder sehr teuer oder schlecht und brauchen pro Scan oft viele Minuten. Einfacher, schneller und oft auch günstiger geht es mit einer Digitalkamera sowie einigen Tools. 
Status: 
featured_image: /images/Beispielfotos/Digitalkamera_vom_Negativ.jpg


Anders als bei anderen gibt es bei mir keinen so üppigen Fundus an
alten Negativen oder gar Dias, bei denen sich eine Digitalisierung
wirklich lohnen würde. Auch sind manche Farbnegative in keinem guten
Zustand.

Allerdings produziere ich durchaus auch heute noch eine gewisse Menge
»analoge« Fotos. Farbige lasse ich meist entwickeln und direkt
digitalisieren. Dafür gibt es Experten, die auf durchaus bezahlbare
Art und Weise eine wunderbare Arbeit abliefern, beispielsweise Jörg
Bergs in Hürtgenwald. Obwohl seine Firma auch S/W-Filme hervorragend
bearbeitet, mache ich das gelegentlich selber.

![Fa Bergs](/images/Beispielfotos/Fa_Bergs.jpg){: .image-process-large-photo}

Wobei die Frage ist, warum? Eigentlich habe ich dafür nur den Grund,
dass ich gerne mit einer Olympus PenFT fotografiere, die genau so alt
ist wie ich selbst. Ausprobiert habe ich dabei zwei Methoden.

## Mit dem Durchlicht-Scanner

Ich nenne einen Epson Perfection 2480 Photo mein eigen. Seit den
nuller Jahren... Das Teil hat eine Durchlichteinheit, die mit xsane
unter Linux ganz ordentlich funktioniert. Bei einer Auflösung von
immerhin 2400 dpi. Das Verfahren ist allerdings zeitraubend und
nervig. Noch dazu macht der Scanner ein Geräusch, das das gesamte
Umfeld einschließlich meiner selbst in einem Maße nervt, dass ich mir
das abgewöhnt habe. Darüber hinaus bleibt einem die aufwändige
Bearbeitung der Rohscans nicht erspart. Die Bilder sind am Ende aber
ganz hübsch. Wenigstens wenn man die Negative von Staub, Fusseln und
Fettfingern frei hält. Aber das gilt ja generell.

![Scanner ...](/images/Beispielfotos/Scanner.jpg){: .image-process-large-photo}

## Mit der Digitalkamera

Bis vor Kurzem hatte ich eine D700, eine bezahlbare Nikon mit einen
Sensor im Kleinbildformat. Dies Format, da ich außer mit der PenFT
auch noch mit einer Nikon F80 auf Film fotografiere. Die vorhandenen
Objektive passen an beiden. ;) Allerdings war mir die D700 etwas zu
alt, so dass sie vor kurzer Zeit einer etwas jüngeren D610 gewichen
ist.

![Digitalkamera vom Dia](/images/Beispielfotos/Digitalkamera_vom_Dia.jpg){: .image-process-large-photo}

Unter den Objektiven gibt es auch ein Makro-Objektiv mit 60 mm
Brennweite. Für diese Brennweite gibt es von Nikon ein Set, das über
eine kleine Mattglasscheibe und einen Schlitz für einen Film-
bzw. Diahalter verfügt. Am anderen Ende ist ein Objektivgewinde, das
auf das genannte Objektiv mit 62 mm Gewindedurchmesser aufgeschraubt
werden kann. Mit einem Adapterring kann das Ganze auch an andere
Gewinde angepasst werden. Die dabei veränderten Abstände können
ausgeglichen werden, der vordere Teil mit der Mattscheibe kann
verschoben und mit einer Rändelschraube fixiert werden. Das Set heißt
»Nikon ES-2 Filmdigitalisierungsadapter«. Man könnte sowas auch aus
schwarzer Pappe basteln, wozu ich jedoch zu faul war.

In der Regel verarbeite ich damit s/w-Material. Dazu brauche ich
sonnige Tage, da ich zur Ausleuchtung keine künstliche Lichtquelle
besitze. Die Blende des Objektivs wird dabei relativ weit zugemacht -
meist auf 11 - und das Ganze nach Sicht händisch scharfgestellt; der
Autofokus verschluckt sich bei dieser Anordnung. Da das Ganze starr
ist, braucht man sich über längere Belichtungszeiten keine Sorgen zu
machen. Damit fotografiere ich einen 36er Film in ca. 10 Minuten
ab. Bei Material von der PenFT hat man natürlich zwei Negative pro
Digitalbild.

![Digitalkamera vom Negativ](/images/Beispielfotos/Digitalkamera_vom_Negativ.jpg){: .image-process-large-photo}

Die Daten von der Kamera bearbeite ich am Rechner weiter. Angefangen
mit geraderücken und schneiden, dann die eigentliche
Bildbearbeitung. Nach dem Invertieren sieht das Bild aus wie ein
Papierabzug von 1920. Ich muss je nach Qualität der Aufnahme
hauptsächlich mit Helligkeit und Kontrast nach Sicht spielen. Und
dabei im Hinterkopf behalten, dass mein Monitor etwas schwammig
ist. Im Großen und Ganzen klappt das aber ganz gut. Das Trägermaterial
von s/w-Filmen ist leicht bläulich. Das kann man bei der Aufnahme
schon loswerden, wenn man einen Amber-Farbfilter dazwischenschaltet.

Natürlich geht das Ganze auch mit Farbmaterial. Das mache ich aber nur
mit Dias, da farbiges Negativmaterial in der Nachbarbeitung für meinen
Geschmack zu aufwändig ist. Selbst wenn man mit einem Hellblau-Filter
das Orange des Trägermaterials schon bei der Aufnahme ausschaltet. Es
gibt wie gesagt Profis, die für nicht allzu üppiges Salär einen
hervorragenden Job machen.

## Fazit

Man kann das machen... Allerdings ist aus meiner Sicht der einzige
wirklich wichtige Grund, altes Material aus der Zeit vor der
Digitalfotografie in die Gegenwart und Zukunft zu retten.

Heutzutage mit einer mechanischen Kamera auf Film zu fotografieren ist
trotz des Hypes, der darum gemacht wird, nicht so richtig
sinnvoll. Ich mache das eigentlich eher aus Spaß an alter Technik. Aus
dem gleichen Grund koche ich auf dem Campingplatz auch auf einem
Petroleumkocher und mache abends Licht und Wärme mit einer
Petromaxlampe. Die F80 habe ich schon länger; sie löste seinerzeit
eine Praktica 1000 TL ab; DSLRs waren mir zu der Zeit noch zu teuer
und meine Tochter wollte den Umgang mit einer SLR lernen.
