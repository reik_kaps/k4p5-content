Title: Analoge Filme digitalisieren (1/2)
Date: 2021-12-25 16:00:00
Modified: 2022-08-22 11:40
Category: fotografie 
Tags: photo, digitalisierung, archivierung
Slug: analoge-fotos-digitalisieren_1
Author: Reiko Kaps
Summary: Alte Negative oder Dias lassen sich mit speziellen Scannern digitaliseren. Die sind entweder sehr teuer oder schlecht und brauchen pro Scan oft viele Minuten. Einfacher, schneller und oft auch günstiger geht es mit einer Digitalkamera sowie einigen quelloffenen Tools. 
Status: 
featured_image: /images/DSC07882.JPG

Zwischen 1985 und 2005 habe ich einige Aktenordner voller Negativfilme
sowie zahlreiche Kisten mit Dias angehäuft. Danach begann bei mir
langsam aber sicher das Zeitalter der digitalen Fotografie. Die alten
Aufnahmen haben aber nicht nur Erinnerungswert für mich, sie gehören
teilweise auch zur Zeitgeschichte. Daher möchte ich diese Aufnahmen
einerseits retten aber auch als digitale Kopie bewahren. Dafür stehen 
inzwischen gleich mehrere Möglichkeiten bereit:

## Dia- und Negativ-Scanner

Diese Spezial-Geräte sind meist sehr teuer (400 Euro und mehr),
stellen dann aber sehr gute Digitalkopien des analogen Filmmaterials
her. Sie scannen oft nur Negative oder Positive, Auflichtvorlagen
beherrschen sie meist nicht. Und je nach Auflösung kann ein Scan
leicht mehrere Minuten dauern - eher ungünstgig, wenn man viele
Aufnahmen digitalisieren will. Hinter [diesem Amazon-Link](https://www.amazon.de/Plustek-Automatischer-Filmnegative-Drittanbietern-Infrarot-Staub/dp/B07ZXZRR24/ref=sr_1_20?keywords=negativscanner&qid=1661161203&sprefix=Negativ%2Caps%2C103&sr=8-20) findet sich
ein Modell aus dieser Geräteklasse.

## Digital Film Scanner

Die Preise dieser Geräte liegen zwischen 50 Euro und 150 Euro. Sie
arbeiten eher wie eine Kamera. Negative oder Dias tasten sie nicht
langwierig zeilenweise ab sondern nehmen das Bild per Kamerasensor in
wenigen Augenblicken auf. Oft besitzen die Geräte zur Voransicht und
Ausrichtung außerdem einen kleinen Bildschirm und lassen sich so auch ohne
Computer bedienen.

Die damit entstandenen digitalen Kopien sind jedoch oft sehr schwammig und
wenig detailreich. Für Internet reichen aber sie aber meist aus. Sie taugen jedoch nicht 
für die digitale Langzeitarchivierung oder als Ausgangspunkt fürs digitale
Remastern der Aufnahmen. 

## Mittels Digitalkamera digitalisieren

Es liegt nahe, das analoge Filmmaterial mit Digitalkameras zu
digitalisieren. Oft sind diese bereits vorhanden, sodass dafür schon
mal keine Extrakosten anfallen. Anders als die genannten Scanner
müssen die Kameras nach der Digitalisierung nicht in den Schrank
wandern und auf den nächsten Einsatz warten. Sie taugen schließlich
auch für herkömmliche Fotografie.

![105mm-Makro-Optik...](/images/DSC07878.JPG){: .image-process-large-photo}

Das 105mm-Makro-Objektiv taugt für viele Einsatzzwecke, sodass sich 
Investition meist bezahlt macht.

Aber nicht jede Digitalkamera ist dieser Aufgabe auch gewachsen:
Besitzt das Gerät kein Makro-Objektiv, lassen sich Dias und Negative
nicht format- bzw. sensorfüllend repoduzieren. Außerdem sollte die
Kamera Belichtungszeit, Blende und Sensor-Empfindlichkeit manuell
einstellen können, was das Feld wiederrum verkleinert. Im Netz finden
sich auch Anleitungen, wie man mittels einfacher Aufbauten auch Handys
für diese Aufgaben einsetzen kann. 

Hier will ich aber nur berichten, wie ich mit schon älteren digitalen
Spiegelreflexkameras und passenden Objektiven analoge Negativ- und
Diafilme digitalisiere. Andere Geräte oder Methoden streife ich nur
oder habe habe sie praktisch ausgeschlossen, da sie nicht meinen
weniger hohen Qualitätsanforderungen gerecht werden. Oder ich fand sie
bereits in meinen Vorüberlegungen derartige unbrauchbar und
unparktikabel, dass ich sie erst gar nicht ausprobiert habe. Dazu
zählen etwa Versuche mit Kompakt- oder Handy-Kameras.

## Hardware
 
Zusätzlich zur Kamera braucht man ein Stativ. Es hält die Kamera ruhig
und fixiert ihre Position. Dabei muss es sich nicht um ein spezielles
Repro-Stativ handeln. Bei normalen Dreibein-Stativen ist lediglich die
anfängliche Ausrichtung aufwendiger.

![Befestigung am Tisch...](/images/DSC07880.JPG){: .image-process-large-photo}

Hat man bereits einen Makro-Schlitten oder scheut dessen Anschaffung
nicht, kann man sich auch eine eigene Stativ-Konstruktion bauen. Bei
mir besteht diese aus zwei Regalbretter und mehreren Bohrungen sowie
einer Klemme, um das ganze am Tisch zu fixieren (Laubsägezwinge). Der
Makro-Schlitten wird mit einer handelsüblichen 1/4 Zoll
Kamera-Schraube am Basisbrett meiner Konstruktion befestigt.


![Beim Ausrichten hilft eine Libelle.](/images/DSC07881.JPG){: .image-process-large-photo}

Desweiteren nutze ich für Negative und Dias eine Durchlicht-Einheit
eines alten Scanners. Diese dürfte inzwischen gut 20 Jahre alt,
verrichtet aber als Leuchttischchen oder eben als Hintergrundbeleuchtung
für die Reproduktion noch immer ihren Dienst. Je nach Format des
Ausgangsmaterials setze ich fertige Negativ-Schienen oder
selbstgebaute Halter ein, die ich etwa aus Lego-Platten improvisiert
habe.

![Halter-Hack](/images/DSC07882.JPG){: .image-process-large-photo}

Für die Aufnahmen empfiehlt sich ein Fernauslöser - je nach Modell
etwa per Kabel oder Infrarot. Man vermeidet damit lässtiges
Wackeln. Besitz man nichts dergleichen, kann man mit dem Zeitauslöser
der Kamera die Aufnahme starten. 

Am liebsten steuere ich die Kamera vom Notebook per USB-Kabel aus:
Zusätzlich werden die Aufnahmen dabei gleich auf den Computer
geladen - umkopieren von SD-Card entfällt.

![Entangle](/images/Entangle-Example.jpg){: .image-process-large-photo}

Für die Fernsteuerung kann man entweder die vom Hersteller
mitgelieferte Software einsetzen, die aber meist nur für Windows und
MacOS bereitsteht. Nutzt man Linux setzt man auf das
Open-Source-Produkt [Entangle](https://entangle-photo.org/), das
vergleichbares leistet sowie einige Extras wie etwa Automatiserung
mitbringt.


 






