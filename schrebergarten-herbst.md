Title: Schrebergärten im Herbst
Date: 2022-08-29 15:32
Modified: 2022-12-04 10:43
Category: fotografie 
Tags: photo, covid, garten
Slug: schrebergarten_2020
Author: Reiko Kaps
Summary: Streifzüge durch Hannovers Schrebergärten im Herbst 
featured_image: /images/IMG_0194.jpg
gallery: {photo}schrebergarten


Die Aufnahmen stammen aus den vergangenen zwei Jahren. Sie enstanden meist auf meinen Rundgängen und Streifzügen, die erst durch das verordnete Homeoffice möglich wurden.
