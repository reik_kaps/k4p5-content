Title: Permanente Einträge in die ARP-Tabelle
Date: 2023-10-03 18:20
Modified: 2023-10-03 18:20
Category: Network
Tags: arp, network, linux, offline
Slug: permanent-arp-entry
Author: Reiko Kaps
Summary: Permanente Einträge in die lokale ARP- bzw. NDP-Tabelle eines Linux-Rechners einfügen
Status: Published
Featured_Image: /images/network.png

Aus diversen Gründen habe ich meinen Homeserver-Pi wieder gegen einen
PC getauscht. Den damit einhergehenden Mehrverbrauch an Strom
minimiere ich durch recht rigide Strommaßnahmen, die ich mittels des
[Autosuspend](https://github.com/languitar/autosuspend "Github-Seite")-Daemons umsetze. 

Das kleine Python-Programm schickt den PC nach einer definierbaren
Zeit in den Ruhestand, überprüft aber vorab, ob eine Reihe von
Ausschlussbedingungen erfüllt sind. Dazu können bestehende SSH- oder
Samba-Verbindungen ebenso gehören wie eigene, selbstgeschrieben Tests
gehören. Außerdem setzt es Wiederaufwach-Timer, den PC entweder in
Intervallen oder zu den Laufzeiten von Systemd-Timern aufwecken.

In meinem Setup schickt Autosuspend den PC nach 15 Minuten Untätikeit
in den S3-Sclaf, weckt ihn zum Backup externe Dienste bzw. alle 5
Stunden wieder auf. Damit ich nicht immer ein Magic-Paket an den
Rechner schicken muss, horcht die Ethernet-Schnittstelle des PCs auf
eingehende Unicast-Pakete. Das funktioniert nur dann, wenn der
anfragende Rechner die MAC-Adresse des PCs über einen dauerhaften
Eintrag in seiner
[ARP-Tabelle](https://de.wikipedia.org/wiki/Address_Resolution_Protocol
"Wikipedia-Eintrag") bzw. NDP-Tabelle kennt.

Unter Linux lässt sich das temporär auf der Kommandozeile mittels des
Befehls **ip** für IPv4 eintragen:  

	ip -i $Schnittstelle -s $Hostname $MAC-Adresse
	
Der Platzhalter $Schnittstelle steht dabei für die
Netzwerkschnittstelle, über die der anzusprechende Rechner ($Hostname)
mit seiner $MAC-Adresse zu erreichen ist. Mittels des Befehls

	ip neighbour show 

bekommt man übrigens sowohl die Einträge für IPv4 als auch IPv6 angezeigt: 

![Befehlsausgabe ip neighbor show](images/ip-neighbor-show.png){: .image-process-large-photo}

Da diese Einträge beim nächsten Boot wieder verschwunden sind, sollte
man diese Einträge beim Start des Netzwerks setzen - etwa mittels
eines [Dispatcher-Skripts des NetworkManagers](https://networkmanager.dev/docs/api/latest/NetworkManager-dispatcher.html). Diese Skripte liegen
meist im Verzeichis __/etc/NetworkManager/dispatcher.d__. Ein Beispiel
dafür sieht etwa so aus: 

	#!/bin/bash
	#
	# run only for specific networks
	if [ "$2" = "up" ]
	then
		echo "[NetworkManager.Dispatcher] activating static arp entries for interface $1"
		/usr/sbin/arp -i $1 -f /etc/ethers
	fi

Das Skript verwendet hier die Option -f, über die sich eine Datei
angeben lässt, die pro Zeile eine MAC-Adresse und den dazu gehörenden
Hostnamen enthält. Das ist pflegeleichter und flexibler, wenn es gilt
neue Einträge hinzu zufügen. Die Shell-Variable $1 enthält hier die
gerade startende ($2) Netzwerkschnittstelle.



