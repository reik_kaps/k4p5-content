Title: E-Science-Tage 2017: Vortrag zu Distributed Research Management
Date: 2017-02-21 07:42
Modified: 2022-08-18 13:37
Author: Reiko Kaps
Tags: Software, Research, p2p, blockchain, ipfs, rdm
Slug: e-science-tage-2017-vortrag-zu-distributed-research-management
Category: Research Data
Summary: Ich halte am 16. März 2017 auf den E-Science-Tagen an der Uni Heidelberg einen Vortrag, der ein verteiltes und dezentrales Modell für den Umgang mit und die Bewahrung von Forschungsdaten skizziert.
featured_image: /images/IPFS.jpg

Am 16. März halte ich auf den E-Science-Tagen an der Uni Heidelberg
einen Vortrag, in dem ich ein verteiltes und dezentrales Modell für
den Umgang mit und die Bewahrung von Forschungsdaten skizziere. Dabei
gehe ich beispielsweise auf [IPFS](https://ipfs.io) ein, das HTTP als
Verbreitungsweg im Internet ablösen will. Es setzt dabei auf
Peer-to-Peer-Techniken und Modelle wie die Blockchain. Es verspricht
zudem Versionierung, eindeutige Zuordnungen und eine belastbare
Infrastruktur, die gegen Ausfälle gewappnet ist. Damit könnten IPFS
oder ähnliche Ansätze Wissenschaftlern helfen, deren
Forschungsergebnisse etwa durch widrige, politische Umstände bedroht
sind: Langwierige und aufwendige Rettungsaktionen, wie man sie gerade
in der USA beobachten kann (vgl. [Data
Refuge](https://www.datarefuge.org/)), wären mit dieser Infrastruktur
unnötig. Folgenden Abstract habe ich bereits im vergangenen Jahr
eingereicht:

Klassisches Forschungsdatenmanagement setzt auf ein Modell, das die
Forschenden eher als Kunden denn als Mitstreiter sieht. Beratung und
Angebote etwa zur Veröffentlichung von Forschungsdaten orientieren
sich an Service-Konzepten, die Informationen zentral verteilen und
vorhalten.

Diese Einstellung spiegelt sich auch bei Forschungsdaten-Repositorien,
die Forschungsergebnisse zwar zentral lagern. Sie eignen sich damit
zwar als Schaufenster, aber weniger als Arbeitsplattform für
Forschungsdaten. Zudem sind derartige Forschungsdaten-Repositorien für
die dringendsten Probleme der Forschenden nur unzureichend gerüstet:
Dazu zählen die Sicherung der Urheberschaft, die Wahrung der
Unversehrtheit von veröffentlichten Daten sowie die dauerhafte
Verknüpfung mit Metadaten und Lizenzinformationen. Diese Repositorien
können die Zusicherungen nur innerhalb ihre Systems
gewährleisten. Verlassen die veröffentlichten Daten diese Refugium,
besteht die Gefahr, dass Urheberschaft, Lizenzinformation und anderes
verloren gehen.

Jenseits dieses im IT-Jagon genannten Client-Server-Ansatzes
existieren jedoch andere Verfahren, Daten und Dienste im Internet
zuverbreiten und deren Authentizität sicherzustellen. Diese
dezentralen Ansätze setzen auf Blockchain-Verfahren, die die
mathematische Grundlage für Kryptowährungen
bilden. Blockchain-Verfahren sichern Transaktionen, stellen
“Einigkeit” unter den beteiligten Knoten her und taugen damit für den
sicheren, vertrauenswürdigen und nachvollziehbaren Austausch
beliebiger Daten – auch Forschungsdaten. So stehen inzwischen
Blockchain-Anwendungen bereit, über die man Dateien dezentral verteilt
(IPFS), Daten strukturiert ablegt (BigchainDB) und untrennbar mit
Metainformationen wie Urheber, Lizenz etc. verbindet
(Mediachain/IPDB). Sie benötigen keine zentrale Instanz, bieten
Verschlüsselung, Zugriffs- sowie Integritätschutz. Jeder kann damit
Daten anbieten und abrufen, aber auch auf dieser Basis eigene
Anwendungen entwickeln.

Der Vortrag möchte diese Ansätze vorstellen, Möglichkeiten aufzeigen
und Herausforderungen dieser Techniken nennen.

[Mein Vortrag wird etwa 20 Minuten dauern und findet am 16. März 2017
um 15:30 Uhr statt.](https://e-science-tage.de/sites/default/files/agenda/E-Science-Tage-2017_Agenda_Detail.pdf)

## Nachtrag 

Wer sich meine alten Einlassungen noch ansehen möchte: [Mein Paper](https://books.ub.uni-heidelberg.de/heibooks/reader/download/285/285-4-79966-2-10-20180118.pdf), den [gesamten Tagungsband](http://dx.doi.org/10.11588/heibooks.285.377) sowie 
die [Vortragsfolien](https://e-science-tage.de/sites/default/files/2017-04/est_talk_kaps_16-03-2017.pdf) findet Ihr hinter den Links.
