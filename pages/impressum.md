Title: Impressum
Date: 2021-11-24 18:12:00
Modified: 2021-11-24 18:27:00
Slug: imprint
Authors: Reiko Kaps

## Legal notices & Privacy policy

Responsible party in terms of the German TDG and data controller in terms of the GDPR:

__Reiko Kaps__

Podbielskistraße 101

30177 Hannover

+49 176 2320 7811

This Web site is a private blog which was designed with privacy in mind.

* upon accessing these Web pages, your Web browser transmits certain data (type of Browser, IP address, possibly referer information) to the server serving theses pages. Certain portions of this data is automatically logged by the server software in order to ensure the site can be operated. In particular the IP address is anonymized. The logs are destroyed after four weeks.

* the site does not set cookies. If you decide to submit a comment, a cookie is set by the commenting system which serves to recognize you as a regular poster and add your details to the commenting box. You can, at any time, choose to remove this cookie by utilizing the function of your Web browser to do so.

* If you use the commenting system, the comments you post are publicly visible and will be indexed by search engines.

* no external resources (fonts, CSS, etc.) are loaded

* these pages do not set tracking information nor does the site do analytics of any kind

* no personal data is collected, processed, or stored on this site.

This site contains links to websites of third parties (“external links”). As the content of these Web sites is not under my control, I cannot assume any liability for such external content. Operators of linked websites shall always be liable for their content and accuracy of the information provided. No illegal contents was recognizable at the time I placed the links.
