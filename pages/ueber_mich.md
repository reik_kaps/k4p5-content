Title: Über mich
Date: 2021-11-05 2:48:00
Modified: 2022-08-18 22:18
Slug: ueber-mich
Author: Reiko Kaps
Summary: Meine Name lautet Reiko Kaps, genannt Reik.

Ich beschäftige mich seit gut 25 Jahren mit Opensource-Software:
Anfangs ging es mir hauptsächlich um Webseiten, später um das
Programmieren von Online-Anwendungen wie Web-Shops. Und dabei fiel
viel zusätzliches Wissen über Netzwerke, Serverbetrieb und
Infrastruktur für mich ab.

Unter anderem damit habe ich anschließend gut 10 Jahre für c't und
heise online geschrieben. Von mir [geschriebene
Artikel](https://www.heise.de/suche/?q=Reiko%20Kaps&rm=search) finden
sich über die Suche auf den Heise-Seiten.

In der Zwischenzeit bin ich wieder in die aktive IT zurückgekehrt: Ich
baue und betreibe für die Uni Hannover ein
[Repositorium](https://data.uni-hannover.de), dass Forschungsdaten
aufbewahrt, beschreibt und veröffentlicht. Dahinter steht ein
Speichersystem, Datenbanken, Webserver und einiges an Netzwerk.

![Reik Kaps](../images/reik_1997.jpg)
