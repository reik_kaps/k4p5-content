Title: Changelog
Date: 2022-08-24 11:44:00
Modified: 2022-08-26 10:58
Slug: changelog
Authors: Reiko Kaps

## 0.0.2 2022-08-25 09:00:00 

### Changed 
 * CSS verändert: Inhalte nun mittig, abhängig von der Auflösung
 * Blöche in Templates verschoben
 
## 0.0.1 2022-08-24 11:44:00 

### Changed 
 * lowtechmagazine Text angepasst.
 * Unnötiges Javascript gelöscht
 
### Added 
 * Anpassungen bei Design und Aufbau

