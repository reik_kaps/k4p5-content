Title: Bookmark-Liste
Date: 2021-11-06 12:48:00
Modified: 2023-05-02 08:23
Slug: link-liste
Authors: Reiko Kaps


## Hacking 

### Spaces

 * [LeineLab](https://leinelab.org)
 * [Stratum0](https://stratum0.org)

### Movements

 * [Homebrewserver-Club](https://homebrewserver.club/)
 * [Lowtechmagazine](https://www.lowtechmagazine.com/)
 
## Linux

### Major Distributions

 * [Debian](https://www.debian.org)
 * [Fedora](https://www.fedora.org)
 * [Arch Linux](https://archlinux.org/)
 * [Rocky Linux](https://rockylinux.org)
 * [Alphine Linux](https://www.alpinelinux.org/)
 
### Mobile Distributions

 * [PostmarketOS](https://postmarketos.org/)
 * [Mobian](https://mobian-project.org/)

## Anwendungen

### Web, Filesharing

 * [Nextcloud](https://nextcloud.com/)
 * [Git Annnex](https://git-annex.branchable.com/)
 * [Apache](https://httpd.apache.org/)
 * [Pelican](https://blog.getpelican.com/)

### Mail, Instant Messaging

 * [NeoMutt](https://neomutt.org)
 * [Profanity](https://profanity-im.github.io/)
 * [Gajim](https://gajim.org/)
 * [Prosody](https://prosody.im/)

### Automation, Management

 * [Ansible](https://www.ansible.com/) 
 * [Chezmoi](https://www.chezmoi.io/)

### Photo, Grafik

 * [Darktable](https://www.darktable.org/)
 * [Gimp](https://www.gimp.org/)

## Programming 

### Tools, Editors

 * [Git](https://git-scm.com/)
 * [Emacs](https://www.gnu.org/software/emacs/)
	 * [Blog von Sacha Shua](https://sachachua.com/blog/)
	 * [Emacs Redux von Bozhidar Batsov](https://emacsredux.com/)
 * [PyCharm Community](https://www.jetbrains.com/pycharm/download)

### [Python](https://www.python.org/)

 * [Python 3 Tutorial](https://docs.python.org/3/tutorial/index.html)
 * [Python Tricks by Dan Bader](https://realpython.com/team/dbader/)
 * [RealPython ](https://realpython.com/)
 
### Shells
 * [Shellhacks](https://www.shellhacks.com)
 * [Suckless](https://suckless.org/)

#### Frameworks

 * [Django](https://www.djangoproject.com/)
 * [Flask](https://flask.palletsprojects.com/en/2.0.x/)
 
 
### [Nim Lang](https://nim-lang.org/)
 
 * [Nim Basics](https://narimiran.github.io/nim-basics/)
 * [Nim by Example](https://nim-by-example.github.io/getting_started/)
 * [Nim Forum](https://forum.nim-lang.org/)
 
### Forth

 * [Deutsche Forth-Gesellschaft](https://wiki.forth-ev.de/doku.php/infos:forthgesellschaftev)
 * [Atari ST FORTH Editor, Interpreter and Compiler by Guillaume Tello](https://gtello.pagesperso-orange.fr/forth_e.htm)
 
## Vintage Computing
 
### Website und Foren

 * [Verein zum Erhalt klassischer Computer e.V.](https://www.classic-computing.org)
   * [VzEkC-Forum](https://forum.classic-computing.de/forum/)
 * [JOYCE-User-AG e.V.](https://www.joyce.de/)
   * [JOYCE-Forum](https://joyceforum.de/)
   
### Classic Computers

#### Amstrad Joyce / PCW 256

 * TODO

#### Amstrad NC 100

 * [ZCN: a CP/M-like operating system](https://zgedneil.nfshost.com/zcn.html)
 * [nc100em; a NC100/200-Emulator](https://zgedneil.nfshost.com/nc100em.html)
 * [Tim's Amstrad NC-Users' Site](https://www.ncus.org.uk)
 
#### Atari ST

 * [EmuTOS: a Free operating system for computers based on Motorola 680x0 or ColdFire microprocessors ](https://emutos.sourceforge.io/)
 * [AtariUpToDate:  versiontracker and software database for all Atari](https://www.atariuptodate.de/)
 * [ Atari Home Forum…](https://forum.atari-home.de)
 
